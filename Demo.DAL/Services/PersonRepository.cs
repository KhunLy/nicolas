﻿using DB;
using Demo.DAL.Entities;
using Demo.DAL.Interfaces;
using Demo.DAL.Mappers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.DAL.Services
{
    class PersonRepository : IRepository<int, Person>
    {
        private Connection _Connection { get; set; }

        public PersonRepository()
        {
            _Connection = new Connection(@"Server=TECHNOBEL, Database=DemoDAL, User Id=sa, Password=test1234=",
                "System.Data.SqlClient");
        }

        public int Create(Person entity)
        {
            Command cmd = new Command("INSERT INTO Person (firstname, lastname, birthdate, mail) OUTPUT inserted.id VALUES ('@firstname', '@lastname', '@birthdate', '@mail');");

            cmd.AddParameter("firstname", entity.Firstname);
            cmd.AddParameter("lastname", entity.Lastname);
            cmd.AddParameter("mail", entity.Mail);
            cmd.AddParameter("birthdate", entity.Birthdate);

            return (int)_Connection.ExecuteScalar(cmd);
        }

        public Person Get(int id)
        {
            Command cmd = new Command("SELECT * FROM Person WHERE Id = @id");
            cmd.AddParameter("@id", id);

            
            return _Connection.ExecuteReader(cmd, DbToEntityMapper.Mapper).SingleOrDefault();
        }

        public IEnumerable<Person> GetAll()
        {
            Command cmd = new Command("SELECT * FROM Person");

            return _Connection.ExecuteReader(cmd, DbToEntityMapper.Mapper);
        }

        public Person Update(Person entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            Command cmd = new Command("DELETE FROM Person WHERE Id = @Id");
            cmd.AddParameter("@Id", id);

            _Connection.ExecuteNonQuery(cmd);
        }
    }
}
