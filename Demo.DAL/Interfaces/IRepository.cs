﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.DAL.Interfaces
{
    public interface IRepository<TKey,TEntity>
         where TEntity : IEntity<TKey>
    {
        //  Create Read Update Delete

        TKey Create(TEntity entity);

        TEntity Get(TKey id);
        IEnumerable<TEntity> GetAll();

        TEntity Update(TEntity entity);

        void Delete(TKey id);
    }
}