﻿using Demo.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.DAL.Mappers
{
    internal class DbToEntityMapper
    {
        public static Person Mapper(IDataReader reader)
        {
            return new Person()
            {
                Id = (int)reader["Id"],
                Firstname = reader["Firstname"].ToString(),
                Lastname = reader["Lastname"].ToString(),
                Mail = reader["Mail"].ToString(),
                Birthdate = (DateTime)reader["Birthdate"]
            };
        }
    }
}
