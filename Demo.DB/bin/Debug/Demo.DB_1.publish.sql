﻿/*
Script de déploiement pour DemoDAL

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "DemoDAL"
:setvar DefaultFilePrefix "DemoDAL"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Début de la régénération de la table [dbo].[Person]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Person] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Firstname] NVARCHAR (50)  NOT NULL,
    [Lastname]  NVARCHAR (50)  NOT NULL,
    [Birthdate] DATETIME2 (7)  NOT NULL,
    [Mail]      NVARCHAR (500) NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Person1] PRIMARY KEY CLUSTERED ([Id] ASC),
    UNIQUE NONCLUSTERED ([Mail] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Person])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Person] ON;
        INSERT INTO [dbo].[tmp_ms_xx_Person] ([Id], [FirstName], [LastName], [Birthdate], [Mail])
        SELECT   [Id],
                 [FirstName],
                 [LastName],
                 [Birthdate],
                 [Mail]
        FROM     [dbo].[Person]
        ORDER BY [Id] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Person] OFF;
    END

DROP TABLE [dbo].[Person];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Person]', N'Person';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Person1]', N'PK_Person', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Mise à jour terminée.';


GO
